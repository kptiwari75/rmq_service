import requests

class ExternalApiTrigger():

    def req_res_data(**kwargs):
        # api-endpoint
        url = "https://reqres.in/api/users"
        params = {}
        r = requests.get(url=url, params=params)
        data = r.json()
        return data
