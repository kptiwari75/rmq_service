import pika
import json
import time
from config.config import *

def get_trigger_handler(data):
    print(data)
    connection = pika.BlockingConnection(pika.ConnectionParameters(host=HOST))
    channel = connection.channel()

    channel.exchange_declare(exchange=EXCHANGE, exchange_type=EXCHANGE_TYPE, durable=DURABLE)

    new_data = data

    channel.basic_publish(exchange=EXCHANGE,
                          routing_key=QUEUE,
                          body=json.dumps(new_data))
    print("%r sent to exchange %r with data: %r" % (ROUTING_KEY, EXCHANGE, new_data))
    connection.close()