HOST = 'localhost'
EXCHANGE = 'xpms_exchange'
EXCHANGE_TYPE = 'topic'
QUEUE = 'reqres'
DURABLE = True
DELIVERY_MODE = 2
ROUTING_KEY = "reqres"